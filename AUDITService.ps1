#================================================================
#Name: AUDITService.ps1
#Auteur: RABOT Alban
#Date: 04/03/2020
#
#Version 1.0
#Description: Script d'audit des status des services AD-DNS-DHCP
#
#================================================================

    #Création du fichier auditService.txt

New-Item -Path 'C:\RésultatAudit\auditService.txt' -ItemType File –Force

    #Création des variables contenant les informations
    
$date = Get-Date
$service_DHCP = Get-Service | Where-Object {$_.Name -eq "Dhcp"} | Out-File C:\RésultatAudit\auditServiceDHCP.txt
$status_DHCP = Get-Content C:\RésultatAudit\auditServiceDHCP.txt

$service_DNS = Get-Service | Where-Object {$_.Name -eq "DNS"} | Out-File C:\RésultatAudit\auditServiceDNS.txt
$status_DNS = Get-Content C:\RésultatAudit\auditServiceDNS.txt

$service_AD = Get-Service | Where-Object {$_.Name -eq "NTDS"} | Out-File C:\RésultatAudit\auditServiceAD.txt
$status_AD = Get-Content C:\RésultatAudit\auditServiceAD.txt


    #Renseignement des informations sur les différents services dans le fichier texte

ADD-content -path "C:\RésultatAudit\auditService.txt" -value $date
ADD-content -path "C:\RésultatAudit\auditService.txt" -value $status_DHCP
ADD-content -path "C:\RésultatAudit\auditService.txt" -value $status_DNS
ADD-content -path "C:\RésultatAudit\auditService.txt" -value $status_AD

    #Suppression du fichier temporaire

Remove-Item -Path 'C:\RésultatAudit\auditServiceDHCP.txt' -Recurse
Remove-Item -Path 'C:\RésultatAudit\auditServiceDNS.txt' -Recurse
Remove-Item -Path 'C:\RésultatAudit\auditServiceAD.txt' -Recurse