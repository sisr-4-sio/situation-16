#=============================================
#Name: AUDITdhcp.ps1
#Auteur: RABOT Alban
#Date: 04/03/2020
#
#Version 1.0
#Description: Script d'audit des étendues DHCP
#
#=============================================

    #Création du fichier auditService.txt

New-Item -Path 'C:\RésultatAudit\auditDHCP.txt' -ItemType File –Force

    #Création des variables contenant les informations
    
$date = Get-Date
$DHCP = Get-DhcpServerv4Scope | Out-File C:\RésultatAudit\auditDHCPtemp.txt
$etendue_DHCP = Get-Content C:\RésultatAudit\auditDHCPtemp.txt

    #Renseignement des informations sur les différents services dans le fichier texte

ADD-content -path "C:\RésultatAudit\auditDHCP.txt" -value $date
ADD-content -path "C:\RésultatAudit\auditDHCP.txt" -value $etendue_DHCP

    #Affichage des étendues avec couleurs

Get-DhcpServerv4Scope |% {if ( $_.state -eq "Inactive") {Write-Host -NoNewLine $_.ScopeId"`t" $_.SubnetMask"`t" $_.Name"`t`t"; Write-host $_.state -ForegroundColor red} elseif ( $_.state -eq "Active") {Write-Host -NoNewLine $_.ScopeId"`t" $_.SubnetMask"`t" $_.Name"`t" ; Write-host $_.state -ForegroundColor green} }

    #Suppression du fichier temporaire

Remove-Item -Path 'C:\RésultatAudit\auditDHCPtemp.txt' -Recurse